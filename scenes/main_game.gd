extends Control

var buttonid:int = -1
var board_prefix = "VBoxContainer/MarginContainer/AspectRatioContainer/GridContainer/Button"
var control_prefix = "VBoxContainer/GridContainer/Button"
var board
var seconds_passed = 0

var firework = preload("res://sprites/firework.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	board = Board.new()
	board.create_board(Global.difficulty_min, Global.difficulty_max)
	
	for i in 81:
		var button:Button = get_node(board_prefix + str(i))
		button.pressed.connect(_game_button_pressed.bind(i))
		var rc = num_to_row_col(i)
		var row = rc[0]
		var col = rc[1]
		var bn = board.get_cell(row, col)
		
		if bn == 0:
			button.text = " "
		else:
			button.text = str(bn)
			button.disabled = true
			#button.theme.set_type_variation("GivenNumbers", "Button")
	
	for i in 9:
		var button = get_node(control_prefix + str(i+1))
		button.pressed.connect(_control_button_pressed.bind(i+1))
		
func _game_button_pressed(num: int):
	print("Button", num, " pressed.")
	buttonid = num
	print(buttonid)
	
func _control_button_pressed(num: int):
	print("here")
	if buttonid != -1:
		var b:Button = get_node(board_prefix + str(buttonid))
		b.text = str(num)
		var rc = num_to_row_col(buttonid)
		board.set_cell(rc[0], rc[1], num)
		
		if board.solved_correctly():
			win()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func num_to_row_col(num: int) -> Array:
	var row = int(num/9)
	var column = num%9
	
	return [row, column]
	
func row_col_to_num(row:int, col:int) -> int:
	return row*9+col

func _on_quit_pressed():
	$ConfirmationDialog.popup()


func _on_timer_timeout():
	seconds_passed += 1
	$VBoxContainer/Label.text = Global.seconds_to_string(seconds_passed)
	$Timer.start(1)


func _on_confirmation_dialog_confirmed():
	get_tree().change_scene_to_file("res://start_menu.tscn")


func _on_control_gui_input(event):
	if event is InputEventMouseButton or event is InputEventScreenTouch:
		get_tree().change_scene_to_file("res://start_menu.tscn")
		
func win():
	if Global.difficulty_min == Global.easy_min or Global.difficulty_min == 80:
		if seconds_passed < Global.easy_fastest:
			Global.easy_fastest = seconds_passed
	elif Global.difficulty_min == Global.medium_min:
		if seconds_passed < Global.medium_fastest:
			Global.medium_fastest = seconds_passed
	elif Global.difficulty_min == Global.hard_min:
		if seconds_passed < Global.hard_fastest:
			Global.hard_fastest = seconds_passed
			
	Global.save_config()
	
	$Congrats/Control.visible = true
	$Timer.stop()
	_on_wintimer_timeout()


func _on_wintimer_timeout():
	for i in 10:
		var vw = firework.instantiate()
		vw.position.x = randi_range(0, get_viewport_rect().size.x)
		vw.position.y = randi_range(0, get_viewport_rect().size.y)
		add_child(vw)
		
	$Congrats/Timer.start(.1)
