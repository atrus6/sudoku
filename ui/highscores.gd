extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.easy_fastest < 1_000_000_000:
		$VBoxContainer/EasyValue.text = Global.seconds_to_string(Global.easy_fastest)
	
	if Global.medium_fastest < 1_000_000_000:
		$VBoxContainer/MediumValue.text = Global.seconds_to_string(Global.medium_fastest)
		
	if Global.hard_fastest < 1_000_000_000:
		$VBoxContainer/HardValue.text = Global.seconds_to_string(Global.hard_fastest)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_pressed():
	get_tree().change_scene_to_file("res://start_menu.tscn")
