extends Node

var difficulty_min = 36
var difficulty_max = 41

var easy_min = 36
var easy_max = 46

var medium_min = 32
var medium_max = 35

var hard_min = 28
var hard_max = 31

var track_one = "res://sounds/cosmic-glow.mp3"
var track_two = "res://sounds/horizon.mp3"
var track_three = "res://sounds/surrealism-ambient-mix.mp3"
var tracks = []
var current_track:int

#Config options
var easy_fastest:int = 1_000_000_000
var medium_fastest:int = 1_000_000_000
var hard_fastest:int = 1_000_000_000
var sound_default = true
var path = "user://config.cfg"

func _ready():
	tracks.append(track_one)
	tracks.append(track_two)
	tracks.append(track_three)
	current_track = randi_range(0,3)
	
	load_config()
	
	if sound_default:
		_on_audio_stream_player_finished()
	
func toggle_sound(toggle:bool):
	if toggle:
		$AudioStreamPlayer.play()
		sound_default = true
		save_config()
	else:
		$AudioStreamPlayer.stop()
		sound_default = false
		save_config()

func _on_audio_stream_player_finished():
	current_track += 1 #Sure, this'll *eventually* overflow.
	$AudioStreamPlayer.stream = load(tracks[current_track%tracks.size()])
	$AudioStreamPlayer.play()
	
func seconds_to_string(total_seconds:int) -> String:
	var minutes = int(total_seconds/60)
	var seconds = total_seconds%60
	
	return str(minutes) + ":" + ("%02d" % seconds)
	
func save_config():
	var config = ConfigFile.new()
	
	config.set_value("User", "easy", easy_fastest)
	config.set_value("User", "medium", medium_fastest)
	config.set_value("User", "hard", hard_fastest)
	config.set_value("User", "sound", sound_default)
	
	config.save(path)
	
func load_config():
	var config = ConfigFile.new()
	var err = config.load(path)
	
	if err != OK:
		return
	
	easy_fastest = config.get_value("User", "easy")
	medium_fastest = config.get_value("User", "medium")
	hard_fastest = config.get_value("User", "hard")
	sound_default = config.get_value("User", "sound")
