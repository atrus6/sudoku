extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	$VBoxContainer/SoundToggle.button_pressed = Global.sound_default


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_easy_button_pressed():
	Global.difficulty_min = Global.easy_min
	Global.difficulty_max = Global.easy_max
	get_tree().change_scene_to_file("res://scenes/main_game.tscn")


func _on_medium_button_pressed():
	Global.difficulty_min = Global.medium_min
	Global.difficulty_max = Global.medium_max
	get_tree().change_scene_to_file("res://scenes/main_game.tscn")


func _on_hard_button_pressed():
	Global.difficulty_min = Global.hard_min
	Global.difficulty_max = Global.hard_max
	get_tree().change_scene_to_file("res://scenes/main_game.tscn")


func _on_fastest_times_pressed():
	get_tree().change_scene_to_file("res://ui/highscores.tscn")


func _on_credits_pressed():
	get_tree().change_scene_to_file("res://credits.tscn")


func _on_sound_toggle_toggled(toggled_on):
	Global.toggle_sound(toggled_on)
