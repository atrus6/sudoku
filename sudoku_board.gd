class_name Board extends Node

var board = []

func _ready():
	clear_board()
		
	#random_board()
	create_board(Global.hard_min, Global.hard_max)
		
	print_board()
	
func create_board(min, max):
	var unique_board = false
	
	while !unique_board:
		clear_board()
		random_board()
		
		var to_remove = 81 - randi_range(min, max)
		
		while to_remove > 0:
			var row = randi_range(0, 8)
			var col = randi_range(0, 8)
			
			if get_cell(row, col) != 0:
				set_cell(row, col, 0)
				to_remove -= 1
		
		unique_board = unique_board()
	
func clear_board():
	board = []
	
	for i in 9:
		board.append([0,0,0,0,0,0,0,0,0])
			
func set_cell(row, column, value):
	board[row][column] = value
	
func get_cell(row, column):
	return board[row][column]
	
func available_numbers(row, column):
	var row_nums = board[row]
	var col_nums = []
	var sector_nums = []
	
	for i in 9:
		col_nums.append(board[i][column])
	
	var column_sector = int(column/3)
	var row_sector = int(row/3)
	for rowi in 3:
		for coli in 3:
			sector_nums.append(board[row_sector*3+rowi][column_sector*3+coli])
			
	var rv = []
	
	for i in range(1,10):
		rv.append(i)
		
	for i in row_nums:
		rv.erase(i)
		
	for i in col_nums:
		rv.erase(i)
		
	for i in sector_nums:
		rv.erase(i)
		
	return rv
	
func set_sector(row, column, value):
	var column_sector = int(column/3)
	var row_sector = int(row/3)
	for rowi in 3:
		for coli in 3:
			board[row_sector*3+rowi][column_sector*3+coli] = value
			
func random_board():
	clear_board()
	var positions = []
	var used_numbers = []
	for row in 9:
		for column in 9:
			positions.append( [row, column] )
			used_numbers.append([])
			
	var pos = 0
	
	while pos < len(positions):
		var avail = available_numbers(positions[pos][0], positions[pos][1])
		
		for num in used_numbers[pos]:
			avail.erase(num)
			
		#print(avail)
		
		if len(avail) < 1:
			for i in range(pos, len(positions)):
				board[positions[i][0]][positions[i][1]] = 0
				used_numbers[i] = []
			pos -= 1
		else:
			var num = avail[randi() % avail.size()]
			board[positions[pos][0]][positions[pos][1]] = num
			used_numbers[pos].append(num)
			pos += 1

func unique_board():
	var old_board = board.duplicate(true)
	var unique = false
	var solved = false
	var positions = []
	
	for row in 9:
		for column in 9:
			positions.append([row, column])
			
	var num_zeros = 0
	
	for position in positions:
		if get_cell(position[0], position[1]) == 0:
			num_zeros += 1
	
	while !solved:
		var current_zeros = num_zeros
		
		for position in positions:
			var row = position[0]
			var col = position[1]
			var num = get_cell(row, col)
			
			if num == 0:
				var possible = available_numbers(row, col)
				
				if len(possible) == 1:
					set_cell(row, col, possible[0])
					num_zeros -= 1
					break
		
		if current_zeros == num_zeros:
			board = old_board
			return false
		elif num_zeros == 0:
			board = old_board
			return true
	
func solved_correctly() -> bool:
	for row in 9:
		for col in 9:
			if get_cell(row, col) == 0:
				return false
	
	for row in board:
		for i in range(1, 9):
			if not row.has(i):
				return false
	
	for col in 9:
		var cn = []
		for row in 9:
			cn.append(get_cell(row, col))
			
		for i in range(1, 9):
			if not cn.has(i):
				return false
				
	var rcv = [1, 5, 8]
	
	for row in rcv:
		var sector_nums = []
		for col in rcv:
			var column_sector = int(col/3)
			var row_sector = int(row/3)
			for rowi in 3:
				for coli in 3:
					sector_nums.append(board[row_sector*3+rowi][column_sector*3+coli])
					
		for i in range(1, 9):
			if not sector_nums.has(i):
				return false
			
	return true
		
func print_board():
	var s = ""
	var row_num = 0
	for row in board:
		if row_num % 3 == 0:
			print("-------------")
		row_num += 1
		s = ""
		var col_num = 0
		for col in row:
			if col_num %3 == 0:
				s = s + "|"
			col_num += 1
			s = s + str(col)
		s = s + "|"
		print(s)
		
	print("-------------")
